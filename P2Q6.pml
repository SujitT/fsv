chan inflow  = [0] of {byte}
chan outflow  = [0] of {byte}
byte liquidLevel;

active proctype Ft1(){
byte n;
end:
inflow ? n;
do
	:: n == 1  -> atomic {printf("Inflow opened , liquid level  %d\n",liquidLevel);
						liquidLevel ++;
						inflow ? n; }
	:: n == 0  -> atomic {printf("Inflow closed\n");
						inflow ? n; }
od;
}



active proctype Ft2(){
byte n;
end:
outflow ? n;
do
	:: n == 1  -> atomic {printf("outflow opened\n");
					liquidLevel --;
						outflow ? n; }
	:: n == 0  -> atomic {printf("outflow closed\n");
						outflow ? n; }
od;
}


active proctype Lt(){
do
::atomic{
if
	:: liquidLevel<40 -> inflow ! 1;
					outflow ! 0;

	:: liquidLevel>60 -> outflow ! 1;
					inflow ! 0;
fi;


}

od;
}



