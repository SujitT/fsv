chan C = [0] of {byte}
chan O1 = [0] of {byte}
chan O2= [0] of {byte}

byte t1 = 4; 

byte t2 = 2;


active proctype generator()
{
end:
	do
	:: C ! 0
	:: C ! 1
	:: C ! 3
	:: C ! 4
	:: C ! 5
	:: C ! 1
	od;

}


active proctype filter(){
byte c;
end:

do
:: C ? c 


if
	:: c >= t1 -> printf("The value C is  channel O1: %d\n",c); O1 ! c;
						
	:: t2 < c < t1 ->  printf("The value C is channelO2 : %d\n",c); O2 ! c;
					
	
	:: c < t2 -> printf("The value C is dropped : %d\n",c);
							
						
fi;


od;

}

active proctype listner(){
byte o1;
byte o2;
end:	
do
	:: O1 ? o1 -> assert(o1 >=t1);
	:: O2 ? o2 -> assert(t2 < o2 < t1);
od;


}

