bool b1= false;
bool b2 = false;
int x = 0;


active proctype P1(){
do
::printf("Process P1() in non-critical\n");
atomic { 
b1 = true;
x =2;

}
do
	:: x == 1 || !b2 -> break;
od;
printf("Process P1() in critical\n");
b1 = false;
od;

}


active proctype P2(){
do
::printf("Process P2() in non-critical\n");
atomic { 
b2 = true;
x =1;

}
do
	:: x == 2 || b1 == false -> break;
od;
printf("Process P2() in critical\n");
b2 = false;
od;
}
