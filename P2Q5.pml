chan channel1 = [0] of {byte}
chan channel2 = [0] of { byte}

active proctype Hello(){
byte n;
end:
do

::atomic{ channel1 ? n ->  printf("Hello \n");
}
od;

}

active proctype World(){
byte n;
end:
do
:: atomic{channel2 ? n ->  printf("World \n");
}
od;


}

active proctype Print(){
do
::atomic{
channel1 ! 1;
channel2 ! 2;
}
od;

}